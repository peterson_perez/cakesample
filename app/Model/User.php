
<?php
	App::uses('AppModel', 'Model');
	class User extends AppModel {

		public $validate = array(
			'name' => array(
				'Please enter your name' => array(
					'rule' => 'notBlank'
					),
				'Name must be at least 2 characters' => array(
					'rule' => array('minLength', 2)
					)
			),
			'username' => array(
				'Username must not have whitespace' => array(
					'rule' => '/^[a-z0-9_\-\.]+$/i'
				),
				'Username already exists' => array(
					'rule' => 'isUnique'
				),
				'Username must be between 5 to 15 characters' => array(
					'rule' => array('between', 5, 15)
				)
				
			),
			'password' => array(
				'Password must be between 5 and 15 characters' => array(
					'rule' => array('between', 5, 15)
				),
				'Passwords do not match' => array(
					'rule' => 'matchPasswords'
				)
			)
		);

		public function matchPasswords($data) {
			if ($data['password'] == $this->data['User']['confirm_password']) {
				return true;
			}
		}

		public function beforeSave() {
			if (isset($this->data['User']['password'])) {
				$this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
			}
			return true;
		}
		
	}