
<?php
	class Post extends AppModel {
		public $validate = array(
			'title' => array(
					'rule' => 'notBlank',
					'message' => 'Please enter a title'	
			),
			'body' => array(
					'rule' => 'notBlank',
					'message' => 'Please enter a body'
			)
		);

		
		public function isOwnedBy($post, $user) {
		    return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
		}
	}