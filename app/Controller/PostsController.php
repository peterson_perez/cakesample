
<?php
	class PostsController extends AppController {

	  public function index() {
	  	$posts = $this->Post->find('all', array(
	  		'conditions' => array(
	  			'Post.user_id' => $this->Auth->user('id')
	  			)
	  		)
	  	);
	  	$this->set(array(
	  		'posts' => $posts, 
	  		'_serialize' => array(
	  			'posts'
	  			)
	  		)
	  	);
	  }

	public function isAuthorized($user) {
	    // All registered users can add posts
	    if ($this->action === 'add') {
	        return true;
	    }

	    // The owner of a post can edit and delete it
	    if (in_array($this->action, array('edit', 'delete'))) {
	        $postId = (int) $this->request->params['pass'][0];
	        if ($this->Post->isOwnedBy($postId, $user['id'])) {
	            return true;
	        }
	    }

	    return parent::isAuthorized($user);
	}

	  public function view($id = null) {
	  	if (!$id) {
	  		throw new NotFoundException(__('Invalid post'));
	  	}	

	  	$post = $this->Post->findById($id);
	  	if (!$post) {
	  		throw new NotFoundException(__('ID is not found'));
	  	}
	  	$this->set('post', $post);
	  }

	  public function add() {
	  	if ($this->request->is('post')) {
	  		$this->request->data['Post']['user_id'] = $this->Auth->user('id');
	  		$this->Post->create();
	  		if ($this->Post->save($this->request->data)) {
	  				$this->Flash->success(__('Your post has been saved.'));
	  				return $this->redirect(array('action' => 'index'));
	  		}
	  		$this->Flash->error(__('Unable to add your post'));
	  	}
	  }

	  public function edit($id = null) {
	  	if (!$id) {
	  		throw new NotFoundException(__('Invalid post'));
	  	}
	  	$post = $this->Post->findById($id);

	  	if (!$post) {
	  		throw new NotFoundException(__('Invalid post'));
	  	}

	  	if ($this->request->is(array('post', 'put'))) {
	  			$this->Post->id = $id;
	  			if ($this->Post->save($this->request->data)) {
	  					$this->Flash->success(__('Your post has been updated.'));
	  					return $this->redirect(array('action' => 'index'));
	  			}
	  			$this->Flash->error(__('Unable to update your post.'));
	  	}

	  	if (!$this->request->data) {
	  			$this->request->data = $post;
	  	}
	  }

	  public function delete($id) {
	  	if ($this->request->is('get')) {
	  		throw new MethodNotAllowedException();
	  	}

	  	if ($this->Post->delete($id)) {
	  			$this->Flash->success(
	  				__('The post has been deleted.')
	  			);
	  	} else {
	  			$this->Flash->error(
	  				__('The post could not be deleted')
	  			);
	  	}

	  	return $this->redirect(array('action' => 'index'));
	  }

	} //End Class
