<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $helpers = array('Html', 'Form');
	public $components = array(
		'Flash',
		'Auth' => array(
			'loginRedirect' => array('controller' => 'users', 'action' =>'index'),
			'logoutRedirect' => array('controller' => 'users', 'action' =>'login'),
			'authError' => "You can't access that page",
			'authorize' => array('Controller')
			)		
	);

	public function beforeFilter() {
		$this->Auth->allow('index', 'view', 'register');
		// $this->Auth->loginRedirect = array('controller' => 'posts', 'action' => 'index');
		// $this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login');
		// $this->set('admin', $this->isAdmin());
	}

	public function isAuthorized($user) {
	    // Admin can access every action
	    if (isset($user['role']) && $user['role'] === 'admin') {
	        return true;
	    }

	    // Default deny
	    return false;
	}
	// public function isAdmin(){
	// 	$admin = false;
	// 	if ($this->Auth->user('role') == 'admin'){
	// 		$admin = true;
	// 	}
	// 	return $admin;
	// }

}
