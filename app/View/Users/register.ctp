<!-- users/register - Create User -->
<div class="users form">

<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo 'Register'; ?></legend>
		<?php 
			echo $this->Form->input('name');
			echo $this->Form->input('username');
			echo $this->Form->input('password');
			echo $this->Form->input('confirm_password', array('type' =>'password')
				);
			// if ($admin) {
			echo $this->Form->input('role', 
				array('options' => array(
					'admin' => 'Admin', 
					'user' => 'User'
						)
					)
				);
			// }
			echo $this->Form->end('Submit');
		 ?>
	 </fieldset>
</div>

<div>
	<?php echo $this->Html->link('Login', array('controller' => 'users', 'action' => 'login')); ?>
</div>
