<!-- File: /app/View/Users/index.ctp -->
<div>
	Welcome<?php ?>
</div>
<br>
<div><?php echo $this->Html->link('Add New User', array('controller' => 'users', 'action' => 'register')); ?></div>
<br>
<div><?php echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout')); ?></div>
<div class="users form">
	<fieldset>
		<legend>Users</legend>
		<table>
			<tr>
				<?php echo $this->Html->tableHeaders(array(
					'Id', 'Name', 'Username', 'Password', 'Role', 'Actions', 'Created'
					)
				); ?>
			</tr>
			<?php foreach ($users as $user): ?>
			<tr>
				<td><?php echo $user['User']['id']; ?></td>
				<td>
					<?php echo $user['User']['name']; ?>
				</td>
				<td>
					<?php echo $user['User']['username']; ?>
				</td>
				<td>
					<?php echo $user['User']['password']; ?>
				</td>
				<td>
					<?php echo $user['User']['role']; ?>
				</td>
				<td>
					<?php echo $this->Html->link('Edit Profile', array(
								'action' => 'edit', $user['User']['id']
								)
							);
					?>
					<?php
					// if ($admin) {
					// 	echo $this->Form->postLink('Delete', array(
					// 			'action' => 'delete', $user['User']['id']), array(
					// 				'confirm' => 'Are you sure you want to delete?')
					// 			);
					// }
					?>

				</td>
				<td>
					<?php echo $user['User']['created']; ?>
				</td>


			</tr>
		 	<?php endforeach; ?>
			<?php unset($user); ?>
		</table>
	</fieldset>
</div>