<!-- File: /app/View/Posts/index.ctp -->

<h1>Blog Posts</h1>
<h1>Welcome! <?php  ?>
</h1>

<table>
	<tr>
		<?php echo $this->Html->tableHeaders(array('ID', 'Title', 'Actions', 'Created')); ?>
	</tr>
	<?php  ?>
	<?php foreach ($posts as $post): ?>
	<tr>
		<td><?php echo $post['Post']['id']; ?></td>
		<td>
			<?php echo $this->Html->link($post['Post']['title'], array('controller' => 'posts', 'action' => 'view', $post['Post']['id'])); 
			?>
		</td>
		<td><?php 
					echo $this->Html->link('Edit', 
						array('action' => 'edit', $post['Post']['id'])
					); 
				?>
				<?php 
					echo $this->Form->postLink('Delete',
						array('action' => 'delete', $post['Post']['id']),
						array('confirm' => 'Are you sure?')
					);
				?>
		</td>
		<td><?php echo $post['Post']['created']; ?></td>
	</tr>
	<?php endforeach; ?>
	<?php unset($post); ?>
</table>

<?php echo $this->Html->link(
    'Add Post',
    array('controller' => 'posts', 'action' => 'add')); 
 ?>
